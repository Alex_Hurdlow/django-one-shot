from django.urls import path
from todos.views import (
    todo_list_list,
    todo_list_details,
    create_todo_list,
    edit_todo_list,
    delete_todo_list,
)


urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", todo_list_details, name="todo_list_details"),
    path("create/", create_todo_list, name="create_todo_list"),
    path("<int:id>/edit/", edit_todo_list, name="edit_todo_list"),
    path("<int:id>/delete/", delete_todo_list, name="delete_todo_list"),
]
