from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoForm


# Create your views here.


def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list,
    }

    return render(request, "todos/todo_list_list.html", context)


def todo_list_details(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {"todo_list_object": todo_list}
    return render(request, "todos/todo_details.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todo_instance = form.save()
            return redirect("todo_list_details", id=todo_instance.id)
    else:
        form = TodoForm()
        context = {"form": form}

    return render(request, "todos/todo_create.html", context)


def edit_todo_list(request, id):
    todo_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_instance)
        if form.is_valid():
            todo_instance = form.save()
            return redirect("todo_list_details", id=todo_instance.id)
    else:
        form = TodoForm(instance=todo_instance)

        context = {"form": form}
    return render(request, "todos/todo_edit.html", context)


def delete_todo_list(request, id):
    todo_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_instance.delete()
        return redirect("todo_list_list")
    return render(request, "todos/todo_delete.html")
